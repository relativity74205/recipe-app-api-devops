variable "prefix" {
  type        = string
  default     = "raad"
  description = "description"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "foo@bar.com"
}

variable "db_username" {
  description = "Username"
}

variable "db_password" {
  description = "Password"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}